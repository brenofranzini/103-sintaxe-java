package tech.mastertech.itau.jogo;

public enum CartaEnum {
	ANTONIO("Antonio", 1), UM("Um", 1), DOIS("Dois", 2), TRES("Tres", 3), QUATRO("Quatro", 4), CINCO("Cinco", 5), SEIS("Seis", 6), SETE("Sete", 7), OITO("Oito", 8), NOVE("Nove", 9), DEZ("Dez", 10), JOAQUIM("Joaquim", 10),
	QUERIDA("Querida", 10), KLEBERSON("Kleberson", 10);

	private int peso;
	private String nome;

	private CartaEnum(String nome, int peso) {
		this.nome = nome;
		this.peso = peso;
	}

	public int getPeso() {
		return peso;
	}
	
	public String getNome() {
		return nome;
	}
}
