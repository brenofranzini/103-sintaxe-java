package tech.mastertech.itau.jogo;

import java.util.List;

public class Exibicao {
	public static void boasVindas() {
		System.out.println("Bem-vindo ao 21 da Mastertech!:");
	}

	public static void menu() {
		System.out.println("Suas opcoes aqui sao:");
		System.out.println("'C': Continuar o jogo e obter mais uma carta");
		System.out.println("'E': Encerrar o jogo e ver a pontuacao");
		System.out.println("'V': Visualizar pontuacao atual");
		System.out.println("'M': Visualizar cartas da mao");
		System.out.println();
		System.out.print("Insira seu comando desejado: ");
	}

	public static void exibirPontuacao(int pontuacao) {
		System.out.print("Sua pontuacao eh: " + pontuacao);
		System.out.println();
		System.out.println();

	}

	public static void exibirCartasDaMao(List<Carta> cartasDaMao) {
		System.out.println("Sua mao eh:");

		for (Carta carta : cartasDaMao) {
			System.out.print("Carta: [" + carta.getNome() + "], ");
			System.out.print("Peso: [" + carta.getValor() + "], ");
			System.out.println("Nipe: [" + carta.nipe + "]");
		}
		System.out.println();
	}
	
	public static void exibirCartaSorteada(Carta carta) {
		System.out.print("Carta: [" + carta.getNome() + "], ");
		System.out.print("Peso: [" + carta.getValor() + "], ");
		System.out.println("Nipe: [" + carta.nipe + "]");
		System.out.println();
	}

}
