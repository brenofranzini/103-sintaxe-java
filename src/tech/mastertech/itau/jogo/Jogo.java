package tech.mastertech.itau.jogo;

import java.util.List;
import java.util.ArrayList;

public class Jogo {
	private static int pontuacao = 0;
	public static List<Carta> cartasDaMao;
	public Baralho baralho;

	public Jogo() {
		cartasDaMao = new ArrayList<>();
	}

	public void jogar() {
		baralho = new Baralho();

		Embaralhador.embaralhar(baralho);

		entregarCarta(baralho);
		entregarCarta(baralho);

		Exibicao.boasVindas();

		pontuacao = calcularPontuacao();

		Exibicao.exibirPontuacao(pontuacao);
		Exibicao.exibirCartasDaMao(cartasDaMao);

		Interacao i = new Interacao();
		String comando = i.interagir();

		System.out.println("Comando: " + comando);
		while (!comando.equals("E")) {
			if (comando.equals("V"))
				Exibicao.exibirPontuacao(pontuacao);
			else if (comando.equals("C")) {
				entregarCarta(baralho);
				pontuacao = calcularPontuacao();
				Exibicao.exibirPontuacao(pontuacao);
			} else if (comando.equals("M")) {
				Exibicao.exibirCartasDaMao(cartasDaMao);
				Exibicao.exibirPontuacao(pontuacao);
			} else
				System.out.println("Comando nao reconhecido");

			if (pontuacao == 21) {
				System.out.println("Parabens cagao, voce ganhou");
				break;
			} else if (pontuacao > 21) {
				System.out.println("Perdeu, vacilao");
				break;
			} else {
				Exibicao.menu();
				comando = i.interagir();
			}
		}

		if(comando.equals("E")) {
			Exibicao.exibirPontuacao(pontuacao);
		}
			
		System.out.println("Fim de jogo");
	}

	public static int calcularPontuacao() {
		pontuacao = 0;

		for (Carta carta : cartasDaMao)
			pontuacao += carta.getValor();

		return pontuacao;
	}

	public void entregarCarta(Baralho baralho) {
//		Exibicao.exibirCartaSorteada(baralho.cartas.get(0));
		cartasDaMao.add(baralho.cartas.get(0));
		baralho.cartas.remove(0);
	}

}
