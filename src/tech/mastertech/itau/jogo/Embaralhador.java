package tech.mastertech.itau.jogo;

import java.util.ArrayList;
import java.util.Collections;

public class Embaralhador {
	
	public static void embaralhar(Baralho baralho)
	{
		Collections.shuffle(baralho.cartas);
	}
}
