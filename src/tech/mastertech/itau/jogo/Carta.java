package tech.mastertech.itau.jogo;

public class Carta {
	private CartaEnum rank;
	public NipeEnum nipe;

	public Carta(CartaEnum rank, NipeEnum nipe) {
		this.rank = rank;
		this.nipe = nipe;
	}

	public int getValor() {
		return rank.getPeso();
	}

	public String getNome() {
		return rank.getNome();
	}
}
