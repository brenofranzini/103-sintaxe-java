package tech.mastertech.itau.jogo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baralho {
	public List<Carta> cartas;

	public Baralho() {
		cartas = new ArrayList<Carta>();
	
		for(NipeEnum naipe : NipeEnum.values()) {
			for(CartaEnum rank : CartaEnum.values()) {
				cartas.add(new Carta(rank, naipe));
			}
		}
	}
}
