package tech.mastertech.itau.exemplos;

public class Application {

	public static void main(String[] args) {
		System.out.println("!");
		teste();
	}

	public static void teste() {
		int[] numeros = { 1, 2, 3 };
		for (int valor : numeros) {
			System.out.println(valor);
		}
	}
}

class Teste {
}