package tech.mastertech.itau.heranca;

public class Professora extends Pessoa implements ServicoDeSecretaria {
	private String profissao = "Professora";

	public String getProfissao() {
		return profissao;
	}
	
	public void ensinar(String conteudo) {
		System.out.println("AE molecada! Hoje a aula é sobre "+conteudo);
	}
}
