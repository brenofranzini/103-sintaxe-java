package tech.mastertech.itau.heranca;

public class App {

	public static void main(String[] args) {
		//Pessoa pessoa = new Pessoa();
		Professora professora = new Professora();

		//System.out.println(pessoa instanceof Professora);
		//System.out.println(professora instanceof Pessoa);
		//System.out.println(professora instanceof Object);
		
		//pessoa.dizerOi();
		professora.ensinar("JAVA");
		professora.dizerOi();
	}

}
